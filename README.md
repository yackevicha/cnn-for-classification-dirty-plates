It's a CNN model for classification plates by dirty and cleaned.

The best result obtained with Resnet-152 (accuracy 0.92338), using transforming with Grayscale, cropping and others.
Also you can get good results with ensemble of different models. I used 5 different models and aggregate average score.
